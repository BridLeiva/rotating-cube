# README #


### SETUP ###

If you use docker just run:

    > docker-compose up
    
else you need to run:

    > bower i
    > npm i
    > npm run watch

Open $project_dir/dist/index.html

Dev Workflow: Make changes to files in src and it will automatically compile. Reload index.html in the browser. 